const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchemea = mongoose.Schema({
    name : {
        type: String,
        required: true,
        trim : true
    },
    email  :{
        type: String,
        required: true,
        trim : true,
        unique : true,
        lowercase : true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid');
                
            }
        }
    },
    password  :{
        type: String,
        required: true,
        trim : true,
        minlength : 8,
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('password connot contain password');
            }
        }
    },
    tokens : [{
        token : {
            type: String,
            required: true
        }
    }]
},{
    timestamps: true
})

// Methods 
userSchemea.methods.toJSON =  function() {
    const user = this;
    const userJson = user.toObject()
    delete userJson.password;
    delete userJson.tokens;
    return userJson;
}

userSchemea.methods.generateAuthToken = async function() {
    const user = this;
    const token =  jwt.sign({_id: user.id.toString()},process.env.JWT_SECRET);
    user.tokens = user.tokens.concat({token});
    await user.save()
    return token;
}

//Static save and post 
userSchemea.statics.findByCredentials =  async (email, password) => {
    const user = await User.findOne({email})
    if(!user) {
        throw new Error('email or password is incorrect')
    }

    const isMatch =  bcrypt.compareSync(password, user.password);
    if(!isMatch){
        throw new Error('email or password is incorrect')
    }
    return user;
}


//Hash password plaintext 
userSchemea.pre('save', async function(next){
    const user = this;
    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password, 8)
    }
    next();
})

const User = mongoose.model('User',userSchemea)
module.exports =  User;