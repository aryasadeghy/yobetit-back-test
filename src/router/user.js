const express = require('express');
const router = new express.Router();
const User = require('./../models/user')
const auth = require('../middleware/auth')
// Register
router.post('/users/register',async (req,res)=> {

    const user = new User(req.body);
    try {
        await user.save();
        res.status(200).send({user})
        
    } catch (error) {
        console.log(error)
        if(error.code == 11000){
            res.status(400).send({error : "User Already Exist"})
        }else{
            res.status(400).send({error : error.message})
        }
    }
})

// Login
router.post('/users/login', async (req,res)=> {
    try {
        const user = await User.findByCredentials(req.body.email,req.body.password)
        const token = await user.generateAuthToken()
        res.status(200).send({user, token})
    } catch (error) {
        res.status(400).send({error :error.message})
    }
})
//logout
router.get('/users/logout', auth, async(req,res)=> {
    try {   
        const userTokens = req.user.tokens;
        console.log(req.user)
        req.user.tokens = userTokens.filter((token) => {
           return token.token !== req.token
        })
        await req.user.save()
        res.status(200).send(req.user)
    } catch (error) {
        res.status(500).send({error : error.message})
    }
});



module.exports =  router;