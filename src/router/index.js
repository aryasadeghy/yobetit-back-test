const express = require('express');
const router = new express.Router();
const userRouter = require('./user')
const regionRouter = require('./region')


router.use(userRouter);
router.use(regionRouter);



module.exports =  router;
