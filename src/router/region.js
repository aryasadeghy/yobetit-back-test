const express = require('express');
const router = new express.Router();
const axios = require("axios");

//Get All 
router.get('/region/country/getAll',async (req,res)=> {
    const name = req.params.name
    const url = 'https://restcountries.eu/rest/v2/all';
    try {
        const response = await axios.get(url);
        const data = response.data;  
        res.status(200).send({data})
    } catch (error) {
        const {response} = error
        const {data} = response
        res.status(response.status).send({error :data.message})
    }
})

// Filter By name
router.get('/region/country/name/:name',async (req,res)=> {
    const name = req.params.name
    const url = 'https://restcountries.eu/rest/v2/name';
    try {
        const response = await axios.get(`${url}/${name}`);
        const data = response.data;  
        res.status(200).send({data})
    } catch (error) {
        const {response} = error
        const {data} = response
        res.status(response.status).send({error :data.message})
    }
})

//Filter By array
router.post('/region/country/searchArray',async (req,res)=> {
    const {filterArray} = req.body
    console.log(Array.isArray(filterArray),'sss')
    const url = 'https://restcountries.eu/rest/v2/all';
    try {
        if(Array.isArray(filterArray)){
            const response = await axios.get(url);
            const data = response.data; 
            const filteredData = []
            filterArray.forEach(filter => {
                data.forEach(item => {
                    if(item.name.toLowerCase().includes(filter.toLowerCase())){
                        filteredData.push(item)
                    }
                })
            })
            res.status(200).send({data :filteredData})
        }else {
            res.status(400).send({error :"input inavlid"})
        }
    } catch (error) {
        const {response} = error
        const {data} = response
        if(response){
            res.status(response.status).send({error :data.message})
        }else {
            res.status(500).send({error :error.message})
        }
    }
})





module.exports =  router;