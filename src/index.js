const express = require('express');
require('./db/mongoose');
const router =  require('./router');
const cors = require('cors')


const app = express();
const port = process.env.PORT|| 3000;

app.use(cors())
app.use(express.json());
app.use('/api',router);

app.get('*',(req,res)=> {
    res.status(200).send({"success" : "It is running successfully"})
})


app.listen(port,()=> {
    console.log(`server is  runnnig on port: ${port} `)
});